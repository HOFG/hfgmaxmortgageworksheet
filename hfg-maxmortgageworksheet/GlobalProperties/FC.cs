﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hfg_maxmortgageworksheet.GlobalProperties
{
    public partial class FC
    {
        public class ControlIds
        {
            #region Refinance
            public const string GroupBox7 = "GroupBox7";
            public const string GroupBox8 = "GroupBox8";
            public const string GroupBox9 = "GroupBox9";
            public const string GroupBox10 = "GroupBox10";
            public const string GroupBox11 = "GroupBox11";
            public const string GroupBox12 = "GroupBox12";

            #region TextBox
            public const string txtRefinanceApplicableLTV = "TextBox28";
            public const string txtRenoCostPCT = "TextBox29";
            public const string txtREFPayoffBalance = "TextBox31";
            public const string txtRefEstimatedValue = "TextBox32";
            public const string txtRefAfterImprovedValue = "TextBox33";
            public const string txtRefMaxCountyLoanLimit = "TextBox34";
            public const string txtRefHardCost = "TextBox35";
            public const string txtRefContingencyReserve = "TextBox36";
            public const string txtRefArchitectEngineerFees = "TextBox37";
            public const string txtRefPlanReviewConsultantFee = "TextBox38";
            public const string txtRefInspections = "TextBox39";
            public const string txtRefTitleUpdates = "TextBox40";
            public const string txtRefPermits = "TextBox41";
            public const string txtRefPaymentReserve = "TextBox42";
            public const string txtRefDrawAdminFee = "TextBox44";
            public const string txtRefTotalRenovationBudget = "TextBox43";
            public const string txtRefRenovationFinanceFees = "TextBox66";
            public const string txtRefTotalFundsRequired = "TextBox48";
            public const string txtRefMaxAllowableBaseLoanAmount = "TextBox49";
            public const string txtRefFinalBaseLoanAmount = "TextBox50";        
            public const string txtRefTotalAlterationImprovementsRepairs = "TextBox55";
            public const string txtRefLand = "TextBox56";
            public const string txRefIncludesDebts = "TextBox57";
            public const string txtRefEstimatedPrepaidItems = "TextBox58";
            public const string txtRefEstimatedClosingCosts = "TextBox59";
            public const string txtRefFinancedPMI = "TextBox60";
            public const string txtRefPMIPaidInCash = "TextBox61";
            public const string txtRefDiscount = "TextBox62";
            public const string txtRefTotalCosts = "TextBox63";
            public const string txtRefSubordinateFinancing = "TextBox64";
            public const string txtRefOtherCredits = "TextBox65";
            public const string txtRefTotalFundsRequired1 = "TextBox45";
            public const string txtRefTotalAlterationImprovementsRepairs1 = "TextBox51";
            public const string txtRefBorrowersOwnFundsForContingencyReserves = "TextBox52";
            public const string txtRefRehabilitationEscrowAccountTot = "TextBox53";
            public const string txtRefTotalContingency = "TextBox54";
            #endregion

            #endregion
            #region Purchase
            public const string GroupBox2 = "GroupBox2";
            public const string GroupBox3 = "GroupBox3";
            public const string GroupBox4 = "GroupBox4";
            public const string GroupBox5 = "GroupBox5";
            public const string GroupBox6 = "GroupBox6";

            #region TextBox
            public const string txtPurchaseApplicableLTV = "TextBox1";
            public const string TextBox2 = "TextBox2";
            public const string txtPurchasePrice = "TextBox4";
            public const string txtEstimatedValue = "TextBox5";
            public const string txtAfterImprovedValue = "TextBox6";
            public const string txtMaxCountyLoanLimit = "TextBox7";
            public const string txtHardCost = "TextBox8";
            public const string txtContingencyReserve = "TextBox9";
            public const string txtArchitectEngineerFees = "TextBox10";
            public const string txtPlanReviewConsultantFee = "TextBox11";
            public const string txtInspections = "TextBox12";
            public const string txtTitleUpdates = "TextBox13";
            public const string txtPermits = "TextBox14";
            public const string txtPaymentReserve = "TextBox15";
            public const string txtDrawAdminFee = "TextBox16";
            public const string txtTotalRenovationBudget = "TextBox17";
            public const string txtRenovationFinanceFees = "TextBox67";
            public const string txtTotalofPurchasePriceImprovements = "TextBox18";
            public const string txtFinalAsCompletedValue = "TextBox19";
            public const string txtTotalOfFinancedPMI = "TextBox21";
            public const string txtMaxAllowableTotalLoanAmount = "TextBox22";
            public const string txtFinalBaseLoanAmount = "TextBox23";
            public const string txtTotalAlterationImprovementsRepairs = "TextBox24";
            public const string txtBorrowersOwnFundsForContingencyReserves = "TextBox25";
            public const string txtRehabilitationEscrowAccountTot = "TextBox26";
            public const string txtTotalContingency = "TextBox27";
            public const string txtMaxAllowableBaseLoanAmount = "TextBox20";
            //6.Title Updates
            #endregion

            #endregion


        }
    }
}
