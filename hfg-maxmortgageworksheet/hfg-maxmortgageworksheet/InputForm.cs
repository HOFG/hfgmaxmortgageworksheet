﻿using EllieMae.Encompass.Forms;
using System;
using winform = System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using EllieMae.Encompass.Collections;
using hfg_maxmortgageworksheet.GlobalProperties;
namespace hfg_maxmortgageworksheet
{
    public class InputForm : Form
    {
        internal GroupBox GroupBox7 = null;
        internal GroupBox GroupBox8 = null;
        internal GroupBox GroupBox9 = null;
        internal GroupBox GroupBox10 = null;
        internal GroupBox GroupBox11 = null;
        internal GroupBox GroupBox12 = null;
        internal TextBox TextBox2 = null;
        FormCalculations Fcalc = new FormCalculations();

        public InputForm()
        {
            this.Load += new EventHandler(Form_load);

        }
        private void Form_load(object sender, EventArgs e)
        {
            if(EncompassApplication.CurrentLoan.Fields["19"].FormattedValue == "Purchase")
            {
                GroupBoxVisible(false);
            }
            else
            {
                GroupBoxVisible(true);
            }
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
            Fcalc.PurchaseSectionA_TextBox1ColorChange(TextBox2);
            Fcalc.PurchaseSectionD_Field1Calculation();
            Fcalc.PurchaseSectionD_Field3Calculation();
            Fcalc.PurchaseSectionD_Field5Calculation();
            Fcalc.PurchaseSectionE_Field3Calculation();
            Fcalc.PurchaseSectionE_Field4Calculation();
            if(EncompassApplication.CurrentLoan.Fields["19"].FormattedValue != "Purchase")
            {
                Fcalc.RefinanceSectionA_Field2Calculation();
                Fcalc.RefinanceSectionB_Field2Calculation();
                Fcalc.RefinanceSectionD_Field1Calculation();
                Fcalc.RefinanceSectionD_Field2Calculation();
                Fcalc.RefinanceSectionD_Field4Calculation();
                Fcalc.RefinanceSectionD_Field5Calculation();
                Fcalc.RefinanceSectionE_Field8Calculation();
                Fcalc.RefinanceSectionE_Field11Calculation();
            }
               
           
        }

        private void GroupBoxVisible(bool visiblestatus)
        {
            GroupBox7.Visible = visiblestatus;
            GroupBox8.Visible = visiblestatus;
            GroupBox9.Visible = visiblestatus;
            GroupBox10.Visible = visiblestatus;
            GroupBox11.Visible = visiblestatus;
            GroupBox12.Visible = visiblestatus;
        }

        public override void CreateControls()
        {
            GroupBox7 = FindControl(FC.ControlIds.GroupBox7) as GroupBox;
            GroupBox8 = FindControl(FC.ControlIds.GroupBox8) as GroupBox;
            GroupBox9 = FindControl(FC.ControlIds.GroupBox9) as GroupBox;
            GroupBox10 = FindControl(FC.ControlIds.GroupBox10) as GroupBox;
            GroupBox11 = FindControl(FC.ControlIds.GroupBox11) as GroupBox;
            GroupBox12 = FindControl(FC.ControlIds.GroupBox12) as GroupBox;
            TextBox2 = FindControl(FC.ControlIds.TextBox2) as TextBox;

        }


        
    }
}
