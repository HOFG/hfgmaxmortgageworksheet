﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winform = System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using EllieMae.Encompass.Collections;
using EllieMae.Encompass.Forms;

namespace hfg_maxmortgageworksheet
{
    public class FormCalculations
    {
        public void SectionC_CalculationForBothPurchaseAndRefinance()
        {
            decimal total = 0;
            decimal x80 = 0;
            decimal renoconters = 0;
            decimal fees = 0;
            decimal planfee = 0;
            decimal insp = 0;
            decimal title = 0;
            decimal permits = 0;
            decimal payment = 0;
            decimal drawfee = 0;
            decimal financefee = 0;
            
            if (EncompassApplication.CurrentLoan.Fields["CX.EDH.RENLTV"].FormattedValue.ToString() == "")
            {
                EncompassApplication.CurrentLoan.Fields["CX.EDH.RENLTV"].Value = ".95";
            }
            
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X80"].FormattedValue.ToString()))
            {
                x80 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X80"].Value);
            }
            //winform.MessageBox.Show("alert1");
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].FormattedValue.ToString()))
            {
                renoconters = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].FormattedValue.ToString()))
            {
                fees = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].FormattedValue.ToString()))
            {
                planfee = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X113"].FormattedValue.ToString()))
            {
                insp = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X113"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X114"].FormattedValue.ToString()))
            {
                title = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X114"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].FormattedValue.ToString()))
            {
                permits = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].FormattedValue.ToString()))
            {
                payment = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOOTHER"].FormattedValue.ToString()))
            {
                drawfee = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOOTHER"].Value);
            }

            

            total = x80 + renoconters + fees + planfee + insp + title + permits + payment + drawfee;
            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].Value = total.ToString();

            //Reno Fees Caculation
            financefee = fees + planfee + insp + title + permits + drawfee;
            EncompassApplication.CurrentLoan.Fields["CX.EHD.AIR.RENOFEES"].Value = financefee.ToString();
        }
        public void PurchaseSectionA_TextBox1ColorChange(TextBox textBox2)
        {
            //In Purchase Section A textbox1 Field Color change calculation

            decimal compValue = 0;
            decimal renoCostPCT = 0;
            bool ppflag = false;
            bool flag356 = false;
            bool contreflag = false;

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue == "")
            {
                ppflag = true;
            }
            else
            {
                ppflag = false;
            }

            if (EncompassApplication.CurrentLoan.Fields["356"].FormattedValue == "")
            {
                flag356 = true;
            }
            else
            {
                flag356 = false;
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue == "")
            {
                contreflag = true;
            }
            else
            {
                contreflag = false;
            }

            if (contreflag == false && flag356 == false && ppflag == false)
            {
                if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue) > 
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["356"].FormattedValue))
                {
                    compValue = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["356"].FormattedValue);
                }
                else
                {
                    compValue = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue);
                }
                if (compValue > 0)
                {
                    renoCostPCT = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue) / compValue) * 100;

                    EncompassApplication.CurrentLoan.Fields["CX.EDH.RENOCOSTPCT"].Value = renoCostPCT.ToString();


                    if (renoCostPCT > 75)
                    {
                        textBox2.BackColor = System.Drawing.Color.MistyRose;
                    }
                    else
                    {
                        textBox2.BackColor = System.Drawing.Color.White;
                    }
                }
            }

        }
        public void PurchaseSectionD_Field1Calculation()
        {

            // Purchase Section D Field 1 Calculation
            decimal frentot = 0;
            decimal f136 = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue != "")
            {
                frentot = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["136"].FormattedValue != "")
            {
                f136 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["136"].FormattedValue);
            }

            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].Value = (frentot + f136);
        }
        public void PurchaseSectionD_Field3Calculation()
        {
            //Purchase Section D Field 3 Calculation

            decimal PPI = 0;
            decimal FinVal = 0;

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue != "")
            {
                PPI = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["356"].FormattedValue != "")
            {
                FinVal = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["356"].FormattedValue);
            }
            decimal ltv = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.EDH.RENLTV"].FormattedValue != "")
            {
                ltv = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EDH.RENLTV"].FormattedValue);
            }
            decimal MCLL = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue != "")
            {
                MCLL = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue);
            }

            //D1 = PPI  D2 = FinVal A1 = ltv B4 = MCLL  D1 or  D2*A1 or B4 or B4

            if (PPI > (FinVal * ltv))
            {
                //if ((FinVal * ltv) > MCLL)
                //{
                //    EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].Value = MCLL;
                //}
                //else
                //{
                    EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].Value = (FinVal * ltv);
                //}
            }
            else
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].Value = PPI;
                //if (PPI < MCLL)
                //{

                //}
                //else
                //{
                //    EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].Value = MCLL;
                //}
            }
        }
        public void PurchaseSectionD_Field5Calculation()
        {
            // Purchase Section D Field 5 calculation       error Field '1045' is read-only
            decimal f1045 = 0;
            decimal fRBLM = 0;
            if (EncompassApplication.CurrentLoan.Fields["1045"].FormattedValue != "")
            {
                f1045 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["1045"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].FormattedValue != "")
            {
                fRBLM = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].FormattedValue);
            }
            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOMTGLNAMT"].Value = (f1045 + fRBLM);
        }
        public void PurchaseSectionE_Field3Calculation()
        {
            //Purchase Section E Field 3 Calculation
            decimal frentot = 0;
            decimal fBOF = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue != "")
            {
                frentot = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue != "")
            {
                fBOF = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue);
            }

            EncompassApplication.CurrentLoan.Fields["CX.REN.ESCROWTOTAL"].Value = (frentot + fBOF);
        }
        public void PurchaseSectionE_Field4Calculation()
        {

            //Purchase Section E Field 4 Calculation 
            decimal fBOF = 0;
            decimal fRCont = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue != "")
            {
                fBOF = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].FormattedValue != "")
            {
                fRCont = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].FormattedValue);
            }
            EncompassApplication.CurrentLoan.Fields["CX.REN.TOTALCONTINGENCY"].Value = (fBOF + fRCont);
        }
        public void RefinanceSectionA_Field2Calculation()
        {

            //Refinance SEction 1 Field 2

            decimal estVal12 = 0;
            decimal b3 = 0;
            decimal c10Reno = 0;
            decimal reCost;

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue != "")
            {
                estVal12 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["356"].FormattedValue != "")
            {
                b3 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["356"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue != "")
            {
                c10Reno = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            if (estVal12 > b3)
            {
                if (b3 != 0)
                {
                    reCost = (c10Reno / b3);
                    EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCOSTPCT"].Value = reCost;
                }
            }
            else
            {
                if (estVal12 != 0)
                {
                    reCost = (c10Reno / estVal12);
                    EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCOSTPCT"].Value = reCost;
                }
            }
        }
        public void RefinanceSectionB_Field2Calculation()
        {

            //Refinalce Section B Field 2 Estimated Value calculation

            decimal estimatedValue;
            decimal actualloanamount = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue.ToString() == "")
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].Value = ".95";
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.TOTALFUNDSREQUIRED"].FormattedValue != "")
            {
                actualloanamount = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.TOTALFUNDSREQUIRED"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue != "")
            {
                estimatedValue = (actualloanamount /
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue));


                EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].Value = estimatedValue;

                //if 1401 is RENO{ Set 1821 to estimated Value

                if (EncompassApplication.CurrentLoan.Fields["1401"].FormattedValue.Contains("RENO"))
                {
                    EncompassApplication.CurrentLoan.Fields["1821"].Value = estimatedValue;
                }
            }
        }
        public void RefinanceSectionD_Field1Calculation()
        {
            //Refinance Section D Field 1 Calculation

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].FormattedValue == "")
            {
                EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].Value = 0;
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue == "")
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].Value = 0;
            }
            var total = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].FormattedValue) +
                                               Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue));

            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].Value = total;
        }
        public void RefinanceSectionD_Field2Calculation()
        {
            decimal b2 = 0;
            decimal b3 = 0;
            decimal smaller = 0;
            decimal b4 = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue != "")
            {
                if (EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue != "")
                {
                    b2 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue) *
                        Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue);
                }
                if (EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue != "") // Was Field 356 20230713 Task 1825
                {
                    b3 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue) * // Was field 356
                        Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue);
                }
            }
            if(EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue != "")
            {
                b4 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue);
            }
            if (b2 > b3)
            {
                smaller = b3;
            }
            else
            {
                smaller = b2;
            }
            if(smaller<b4)
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = smaller;
            }else
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = b4;
            }

        }
        public void RefinanceSectionD_Field4Calculation()
        {
            //Refinace Section D Field 4 Calculation

            //if (EncompassApplication.CurrentLoan.Fields["CX.REN.RIF.CL.CCP"].FormattedValue == "")
            //{
            //    EncompassApplication.CurrentLoan.Fields["CX.REN.RIF.CL.CCP"].Value = 0;
            //}

            //if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].FormattedValue == "")
            //{
            //    EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].Value = 0;
            //}

            //if (EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].FormattedValue == "")
            //{
            //    EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value = 0;
            //}

            //var total = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.RIF.CL.CCP"].FormattedValue) +
            //            Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].FormattedValue) -
            //            Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].FormattedValue);

            //EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOACTUALLOANAMOUNT"].Value = total;
        }
        public void RefinanceSectionD_Field5Calculation()
        {

            //Refinance Section D Field 5 Calculation

            //decimal estVal = 0;
            //decimal ltv = 0;
            //decimal MCLL = 0;
            //decimal TotalFund = 0;
            //if (EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue != "")
            //{
            //    estVal = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue);
            //}
            //if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue != "")
            //{
            //    ltv = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue);
            //}
            //if (EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue != "")
            //{
            //    MCLL = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue);
            //}
            //if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOACTUALLOANAMOUNT"].FormattedValue != "")
            //{
            //    TotalFund = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOACTUALLOANAMOUNT"].FormattedValue);
            //}
            //if ((estVal * ltv) > MCLL)
            //{
            //    if (MCLL > TotalFund)
            //    {
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = TotalFund;
            //    }
            //    else
            //    {
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = MCLL;
            //    }
            //}
            //else
            //{
            //    if ((estVal * ltv) > TotalFund)
            //    {
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = TotalFund;
            //    }
            //    else
            //    {
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = (estVal * ltv);
            //    }
            //}
        }
        public void RefinanceSectionE_Field8Calculation()
        {
            //Refinace Section E Field 8 Calculation     

            decimal frentot = 0;
            decimal f968 = 0;
            decimal frefi = 0;
            decimal f138 = 0;
            decimal f137 = 0;
            decimal f1045 = 0;
            decimal f337 = 0;
            decimal f1093 = 0;

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue != "")
            {
                frentot = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["968"].FormattedValue != "")
            {
                f968 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["968"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].FormattedValue != "")
            {
                frefi = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["138"].FormattedValue != "")
            {
                f138 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["138"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["137"].FormattedValue != "")
            {
                f137 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["137"].FormattedValue);
            }



            if (EncompassApplication.CurrentLoan.Fields["337"].FormattedValue != "")
            {
                f337 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["337"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["1093"].FormattedValue != "")
            {
                f1093 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["1093"].FormattedValue);
            }
            //System.Windows.Forms.MessageBox.Show((frentot + f968 + frefi + f138 + f137 + f337 + f1093).ToString());
            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].Value = (frentot + f968 + frefi + f138 + f137 + f337 + f1093);
        }
        public void RefinanceSectionE_Field11Calculation()
        {
            //Refinance Section E field 11 Calculation
            decimal rtcs = 0;
            decimal u149 = 0;
            decimal rbfs = 0;

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].FormattedValue != "")
            {

                rtcs = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].FormattedValue);

            }


            if (EncompassApplication.CurrentLoan.Fields["URLA.X149"].FormattedValue != "")
            {
                u149 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["URLA.X149"].FormattedValue);
            }


            if (EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue != "") //added rbfs 20230713 Task 1825
            {
                rbfs = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue);
            }


            EncompassApplication.CurrentLoan.Fields["CX.EHD.TOTALFUNDSREQUIRED"].Value = (rtcs - u149 - rbfs );
        }
    }
}
