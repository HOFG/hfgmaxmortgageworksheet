﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hfg_maxmortgageworksheet.GlobalProperties
{
    public partial class FC
    {
        public class ControlIds
        {
            public const string GroupBox7 = "GroupBox7";
            public const string GroupBox8 = "GroupBox8";
            public const string GroupBox9 = "GroupBox9";
            public const string GroupBox10 = "GroupBox10";
            public const string GroupBox11 = "GroupBox11";
            public const string GroupBox12 = "GroupBox12";

            public const string TextBox2 = "TextBox2";
        }
    }
}
