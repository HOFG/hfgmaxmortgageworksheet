﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winform = System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using EllieMae.Encompass.Collections;
using EllieMae.Encompass.Forms;

namespace hfg_maxmortgageworksheet
{
    public class FormCalculations
    {
        public void SectionC_CalculationForBothPurchaseAndRefinance()
        {
            decimal total = 0;
            decimal x80 = 0;
            decimal renoconters = 0;
            decimal fees = 0;
            decimal planfee = 0;
            decimal insp = 0;
            decimal title = 0;
            decimal permits = 0;
            decimal payment = 0;
            decimal drawfee = 0;
            decimal financefee = 0;
          //  decimal purch = 0;
            //winform.MessageBox.Show("1");
            //if (string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EDH.RENLTV"].FormattedValue.ToString()))
            //{
            //    EncompassApplication.CurrentLoan.Fields["CX.EDH.RENLTV"].Value = ".95";
            //}
            //winform.MessageBox.Show("2");
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X80"].FormattedValue.ToString()))
            {
                x80 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X80"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].FormattedValue.ToString()))
            {
                renoconters = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].FormattedValue.ToString()))
            {
                fees = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].FormattedValue.ToString()))
            {
                planfee = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X113"].FormattedValue.ToString()))
            {
                insp = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X113"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X114"].FormattedValue.ToString()))
            {
                title = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X114"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].FormattedValue.ToString()))
            {
                permits = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].FormattedValue.ToString()))
            {
                payment = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].Value);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOOTHER"].FormattedValue.ToString()))
            {
                drawfee = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOOTHER"].Value);
            }
          
            


            total = x80 + renoconters + fees + planfee + insp + title + permits + payment + drawfee; 
            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].Value = total.ToString();

            //Reno Fees Caculation
            financefee = fees + planfee + insp + title + permits + drawfee;
            EncompassApplication.CurrentLoan.Fields["CX.EHD.AIR.RENOFEES"].Value = financefee.ToString();
        }
        public void PurchaseSectionA_TextBox1ColorChange(TextBox textBox2)
        {
            //In Purchase Section A textbox1 Field Color change calculation

            decimal compValue = 0;
            decimal renoCostPCT = 0;
            bool ppflag = false;
            bool flag356 = false;
            bool contreflag = false;

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue == "")
            {
                ppflag = true;
            }
            else
            {
                ppflag = false;
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue == "") // was 356
            {
                flag356 = true;
            }
            else
            {
                flag356 = false;
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue == "")
            {
                contreflag = true;
            }
            else
            {
                contreflag = false;
            }

            if (contreflag == false && flag356 == false && ppflag == false)
            {
                if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue) >
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue)) // was 356
                {
                    compValue = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue); // was 356
                }
                else
                {
                    compValue = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue);
                }
                if (compValue > 0)
                {
                    renoCostPCT = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue) / compValue) * 100;

                    EncompassApplication.CurrentLoan.Fields["CX.EDH.RENOCOSTPCT"].Value = renoCostPCT.ToString();


                    if (renoCostPCT > 75)
                    {
                        textBox2.BackColor = System.Drawing.Color.MistyRose;
                    }
                    else
                    {
                        textBox2.BackColor = System.Drawing.Color.White;
                    }
                }
            }

        }

        public void PurchaseSectionB_Field2Calculation()
        {
            //136 //B1
            //CX.EHD.RENOTOTAL   //C10
            //CX.EHD.RENOTOTALPURCHPRICE   //B2
            decimal b1 = 0;
            decimal c10 = 0;
            decimal b2 = 0;
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["136"].FormattedValue))
            {
                b1 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["136"].FormattedValue);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue))
            {
                c10 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            try
            {
                b2 = b1 + c10;
            }
            catch(Exception )
            {
                b2 = 0;
            }
            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].Value = b2;
        }

        public void RefinanceSectionF_Field4Calculation()
        {
            //CX.REN.TOTALCONTINGENCY //F4
            //CX.REN.BOROWNFUNDS //F2
            //CX.EHD.RENOCONTRES //C2
            decimal f4 = 0;
            decimal f2 = 0;
            decimal c2 = 0;
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].FormattedValue))
            {
                c2 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].FormattedValue);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue))
            {
                f2 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue);
            }
            f4 = c2 + f2;
            //winform.MessageBox.Show(f2.ToString());
            EncompassApplication.CurrentLoan.Fields["CX.REN.TOTALCONTINGENCY"].Value = f4;

        }

        public void RefinanceSectionF_Field3Calculation()
        {
            //CX.EHD.RENOTOTAL //F1
            //CX.REN.BOROWNFUNDS//F2
            decimal f1 = 0;
            decimal f2 = 0;
            decimal f3 = 0; //CX.REN.ESCROWTOTAL
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue))
            {
                f1 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue))
            {
                f2 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue);
            }
            f3 = f1 + f2;
            EncompassApplication.CurrentLoan.Fields["CX.REN.ESCROWTOTAL"].Value = f3;
        }

        public void PurchaseSectionA_Field2Calculation()
        {
            //CX.EHD.RENOTOTAL: //C10
            //356 //b3
            //CX.EHD.RENOTOTALPURCHPRICE: //B2
            //CX.EDH.RENOCOSTPCT: //A2
            decimal b2 = 0;
            decimal b3 = 0;
            decimal c10 = 0;
            decimal a2 = 0;

            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue.ToString()))
            {
                b2 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue);
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue.ToString())) // was 356
            {
                b3 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue); // was 356
            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue.ToString()))
            {
                c10 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            try
            {
                if (b2 < b3)
                {
                    a2 = (c10 / b2) * 100;
                }
                else
                {
                    a2 = (c10 / b3) * 100;
                }
            }
            catch (Exception )
            {
                a2 = 0;
            }
            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCOSTPCT"].Value = a2;

        }

        public void PurchaseSectionD_Field1Calculation()
        {

            // Purchase Section D Field 1 Calculation
            decimal frentot = 0;
            decimal f136 = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue != "")
            {
                frentot = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["136"].FormattedValue != "")
            {
                f136 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["136"].FormattedValue);
            }

            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].Value = (f136 + frentot - f136); 
           
        }
        public void PurchaseSectionD_Field3Calculation()
        {
            //Purchase Section D Field 3 Calculation
            //CX.EDH.RENLTV: //A1
            //CX.EHD.RENOTOTALPURCHPRICE: //B2  
            //356 //B3
            //CX.REN.HSMAXLMT: //B4
            decimal b2 = 0; //B2
            decimal b3 = 0; //B3
            decimal ltv = 0;  //A1
            decimal b4 = 0; //B4

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue != "")
            {
                b2 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALPURCHPRICE"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue != "") // was 356
            {
                b3 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue); // was 356
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EDH.RENLTV"].FormattedValue != "")
            {
                ltv = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EDH.RENLTV"].FormattedValue)/100;
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue != "")
            {
                b4 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue);
            }

            //D1 = PPI  D2 = FinVal A1 = ltv B4 = MCLL  D1 or  D2*A1 or B4 or B4

            if ((b3 * ltv) > (b4))
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].Value = b4;
                
            }


           else if ((b2 * ltv) > (b3 * ltv)) 
           // if (b2 > b3)
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].Value = b3 * ltv;
              
            }

            
            else if ((b3 * ltv) > (b4))     
                
                    {
                         EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].Value = (b4);
                
                      }
            
            else
            {
                    EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].Value =(b2 * ltv);
                 
                }
            }
      
    public void PurchaseSectionD_Field5Calculation()
    {
            // Purchase Section D Field 5 calculation       error Field '1045' is read-only
            decimal f1045 = 0;
            decimal fRBLM = 0;
            if (EncompassApplication.CurrentLoan.Fields["1045"].FormattedValue != "")
            {
                f1045 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["1045"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].FormattedValue != "")
            {
                fRBLM = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOBASELOANAMT"].FormattedValue);
            }
            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOMTGLNAMT"].Value = (f1045 + fRBLM);
        }
        public void PurchaseSectionE_Field3Calculation()
        {
            //Purchase Section E Field 3 Calculation
            decimal frentot = 0;
            decimal fBOF = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue != "")
            {
                frentot = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue != "")
            {
                fBOF = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue);
            }

            EncompassApplication.CurrentLoan.Fields["CX.REN.ESCROWTOTAL"].Value = (frentot + fBOF);
        }
        public void PurchaseSectionE_Field4Calculation()
        {

            //Purchase Section E Field 4 Calculation 
            decimal fBOF = 0;
            decimal fRCont = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue != "")
            {
                fBOF = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].FormattedValue != "")
            {
                fRCont = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].FormattedValue);
            }
            EncompassApplication.CurrentLoan.Fields["CX.REN.TOTALCONTINGENCY"].Value = (fBOF + fRCont);
        }
        public void RefinanceSectionA_Field2Calculation()
        {

            //Refinance SEction 1 Field 2

            //decimal estVal12 = 0;
            decimal b3 = 0;
            decimal c10Reno = 0;
           // decimal reCost;

            //if (EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue != "")
            //{
            //    estVal12 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue);
            //}

            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue)) //Was field 356 20230713 Task 1825
            {
                b3 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue);  //Was field 356 20230713 Task 1825
            }

            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue))
            {
                c10Reno = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }
            try
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCOSTPCT"].Value = (c10Reno / b3) * 100;
            }
            catch(Exception )
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCOSTPCT"].Value = 0;
            }
            //if (estVal12 > b3)
            //{
            //    if (b3 != 0)
            //    {
            //        reCost = (c10Reno / b3);
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCOSTPCT"].Value = reCost;
            //    }
            //}
            //else
            //{
            //    if (estVal12 != 0)
            //    {
            //        reCost = (c10Reno / estVal12);
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCOSTPCT"].Value = reCost;
            //    }
            //}
        }
        public void RefinanceSectionB_Field2Calculation()
        {

            //Refinalce Section B Field 2 Estimated Value calculation
            //CX.EHD.TOTALFUNDSREQUIRED: //E11
            //CX.EHD.RENOLTV:   //A1
            //CX.REN.PI.ESTVALUE:  //B2
            decimal e11 = 0;
            decimal a1 = 0;  
            decimal b2 = 0;

            //decimal estimatedValue;
            //decimal actualloanamount = 0;
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue))
            {
                a1 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue);
               

            }
            if (!string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["CX.EHD.TOTALFUNDSREQUIRED"].FormattedValue))
            {
                e11 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.TOTALFUNDSREQUIRED"].FormattedValue);
            }
            try
            {
                b2 = (e11 / a1) * 100;
            }
            catch (Exception ) // was Exception.ex  20230726
            {
                b2 = 0;
            }
            EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].Value = b2;
            //if (EncompassApplication.CurrentLoan.Fields["CX.EHD.TOTALFUNDSREQUIRED"].FormattedValue != "")
            //{
            //    actualloanamount = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.TOTALFUNDSREQUIRED"].FormattedValue);
            //}

            //if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue != "")
            //{
            //    estimatedValue = (actualloanamount /
            //        Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue));


            //    EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].Value = estimatedValue;

            //    //if 1401 is RENO{ Set 1821 to estimated Value

            //    //if (EncompassApplication.CurrentLoan.Fields["1401"].FormattedValue.Contains("RENO"))
            //    //{
            //    //    EncompassApplication.CurrentLoan.Fields["1821"].Value = estimatedValue;
            //    //}
            //}
        }
        public void RefinanceSectionD_Field1Calculation()
        {
            //Refinance Section D Field 1 Calculation

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].FormattedValue == "")
            {
                EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].Value = 0;
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue == "")
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].Value = 0;
            }
            var total = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].FormattedValue) +
                                               Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue));

            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].Value = total;
        }
        public void RefinanceSectionD_Field2Calculation()
        {
            decimal b2 = 0; //CX.REN.PI.ESTVALUE
            decimal b3 = 0;
          //  decimal smaller = 0;
            decimal b4 = 0;
           // decimal d1 = 0; //CX.EHD.TOTALFUNDSREQUIRED
            decimal ltv = 0;

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue != "")
                ltv = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue) / 100;
            {
               
                if (EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue != "")   //Was field 356 20230713 Task 1825
                {
                    b3 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIAV"].FormattedValue);  //Was field 356 20230713 Task 1825

                }
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue != "")
            {
                b4 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue != "")
            {
                b2 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue); 
            }
         //   if (b2 > b3 * ltv)
          //  {
         //       EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = b3 * ltv;
           //     EncompassApplication.CurrentLoan.Fields["CX.B3"].Value = b3 * ltv;
          //  }

            if ((b3 * ltv) > (b4))
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = b4;
              //  EncompassApplication.CurrentLoan.Fields["CX.D1"].Value = b4;

            }

            else if ((b2 * ltv) > (b3 * ltv))
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = b3 * ltv;
               // EncompassApplication.CurrentLoan.Fields["CX.B3"].Value = b3 * ltv;

            }
            else if ((b3 * ltv) > (b4))
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = b4;
               // EncompassApplication.CurrentLoan.Fields["CX.D1"].Value = b4;
            }

            else
            {
                EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = b2 * ltv;
               // EncompassApplication.CurrentLoan.Fields["CX.D1"].Value = b2;

            }


        }
        public void RefinanceSectionD_Field4Calculation()
        {
            //Refinace Section D Field 4 Calculation

            //if (EncompassApplication.CurrentLoan.Fields["CX.REN.RIF.CL.CCP"].FormattedValue == "")
            //{
            //    EncompassApplication.CurrentLoan.Fields["CX.REN.RIF.CL.CCP"].Value = 0;
            //}

            //if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].FormattedValue == "")
            //{
            //    EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].Value = 0;
            //}

            //if (EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].FormattedValue == "")
            //{
            //    EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value = 0;
            //}

            //var total = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.RIF.CL.CCP"].FormattedValue) +
            //            Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].FormattedValue) -
            //            Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].FormattedValue);

            //EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOACTUALLOANAMOUNT"].Value = total;
        }
        public void RefinanceSectionD_Field5Calculation()
        {

            //Refinance Section D Field 5 Calculation

            //decimal estVal = 0;
            //decimal ltv = 0;
            //decimal MCLL = 0;
            //decimal TotalFund = 0;
            //if (EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue != "")
            //{
            //    estVal = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.PI.ESTVALUE"].FormattedValue);
            //}
            //if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue != "")
            //{
            //    ltv = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOLTV"].FormattedValue);
            //}
            //if (EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue != "")
            //{
            //    MCLL = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.HSMAXLMT"].FormattedValue);
            //}
            //if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOACTUALLOANAMOUNT"].FormattedValue != "")
            //{
            //    TotalFund = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOACTUALLOANAMOUNT"].FormattedValue);
            //}
            //if ((estVal * ltv) > MCLL)
            //{
            //    if (MCLL > TotalFund)
            //    {
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = TotalFund;
            //    }
            //    else
            //    {
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = MCLL;
            //    }
            //}
            //else
            //{
            //    if ((estVal * ltv) > TotalFund)
            //    {
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = TotalFund;
            //    }
            //    else
            //    {
            //        EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOREFIBASELOANAMT"].Value = (estVal * ltv);
            //    }
            //}
        }
        public void RefinanceSectionE_Field8Calculation()
        {
            //Refinace Section E Field 8 Calculation     

            decimal frentot = 0;
            decimal f968 = 0;
            decimal frefi = 0;
            decimal f138 = 0;
            decimal f137 = 0;
           // decimal f1045 = 0;
            decimal f337 = 0;
            decimal f1093 = 0;

            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue != "")
            {
                frentot = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTAL"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["968"].FormattedValue != "")
            {
                f968 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["968"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].FormattedValue != "")
            {
                frefi = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.REFIMTPO"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["138"].FormattedValue != "")
            {
                f138 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["138"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["137"].FormattedValue != "")
            {
                f137 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["137"].FormattedValue);
            }



            if (EncompassApplication.CurrentLoan.Fields["337"].FormattedValue != "")
            {
                f337 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["337"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["1093"].FormattedValue != "")
            {
                f1093 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["1093"].FormattedValue);
            }
            //System.Windows.Forms.MessageBox.Show((frentot + f968 + frefi + f138 + f137 + f337 + f1093).ToString());
            EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].Value = (frentot + f968 + frefi + f138 + f137 + f337 + f1093);
        }
        public void RefinanceSectionE_Field11Calculation()
        {
            //Refinance Section E field 11 Calculation
            decimal rtcs = 0;
            decimal u149 = 0;
            decimal rbfs = 0; //added rbfs 20230713 Task 1825


            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].FormattedValue != "")
            {

                rtcs = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOTOTALCOSTS"].FormattedValue);

            }


            if (EncompassApplication.CurrentLoan.Fields["URLA.X149"].FormattedValue != "")
            {
                u149 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["URLA.X149"].FormattedValue);
            }

            if (EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue != "") //added rbfs 20230713 Task 1825
            {
                rbfs = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].FormattedValue);
            }



            EncompassApplication.CurrentLoan.Fields["CX.EHD.TOTALFUNDSREQUIRED"].Value = (rtcs - u149 - rbfs); //added rbfs 20230713 Task 1825
        }
    }
}
