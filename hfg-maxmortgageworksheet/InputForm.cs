﻿using EllieMae.Encompass.Forms;
using System;
using winform = System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using EllieMae.Encompass.Collections;
using hfg_maxmortgageworksheet.GlobalProperties;
namespace hfg_maxmortgageworksheet
{
    public class InputForm : Form
    {
        internal GroupBox GroupBox7 = null;
        internal GroupBox GroupBox8 = null;
        internal GroupBox GroupBox9 = null;
        internal GroupBox GroupBox10 = null;
        internal GroupBox GroupBox11 = null;
        internal GroupBox GroupBox12 = null;

        #region RefinanceTextBox
        internal TextBox txtRefinanceApplicableLTV = null;
        internal TextBox txtRenoCostPCT = null;
        internal TextBox txtREFPayoffBalance = null;
        internal TextBox txtRefEstimatedValue = null;
        internal TextBox txtRefAfterImprovedValue = null;
        internal TextBox txtRefMaxCountyLoanLimit = null;
        internal TextBox txtRefHardCost = null;
        internal TextBox txtRefContingencyReserve = null;
        internal TextBox txtRefArchitectEngineerFees = null;
        internal TextBox txtRefPlanReviewConsultantFee = null;
        internal TextBox txtRefInspections = null;
        internal TextBox txtRefTitleUpdates = null;
        internal TextBox txtRefPermits = null;
        internal TextBox txtRefPaymentReserve = null;
        internal TextBox txtRefDrawAdminFee = null;
        internal TextBox txtRefTotalRenovationBudget = null;
        internal TextBox txtRefRenovationFinanceFees = null;
        internal TextBox txtRefTotalFundsRequired = null;
        internal TextBox txtRefMaxAllowableBaseLoanAmount = null;
        internal TextBox txtRefFinalBaseLoanAmount = null;
        internal TextBox txtRefTotalAlterationImprovementsRepairs = null;
        internal TextBox txtRefLand = null;
        internal TextBox txRefIncludesDebts = null;
        internal TextBox txtRefEstimatedPrepaidItems = null;
        internal TextBox txtRefEstimatedClosingCosts = null;
        internal TextBox txtRefFinancedPMI = null;
        internal TextBox txtRefPMIPaidInCash = null;
        internal TextBox txtRefDiscount = null;
        internal TextBox txtRefTotalCosts = null;
        internal TextBox txtRefSubordinateFinancing = null;
        internal TextBox txtRefOtherCredits = null;
        internal TextBox txtRefTotalFundsRequired1 = null;
        internal TextBox txtRefTotalAlterationImprovementsRepairs1 = null;
        internal TextBox txtRefBorrowersOwnFundsForContingencyReserves = null;
        internal TextBox txtRefRehabilitationEscrowAccountTot = null;
        internal TextBox txtRefTotalContingency = null;
        #endregion

        #region PurchaseGroupControl
        internal GroupBox GroupBox2 = null;
        internal GroupBox GroupBox3 = null;
        internal GroupBox GroupBox4 = null;
        internal GroupBox GroupBox5 = null;
        internal GroupBox GroupBox6 = null;
        #endregion
        #region PurchaseTextboxes
        internal TextBox TextBox2 = null;
        internal TextBox txtPurchaseApplicableLTV = null;
        internal TextBox txtPurchasePrice = null;
        internal TextBox txtEstimatedValue = null;
        internal TextBox txtAfterImprovedValue = null;
        internal TextBox txtMaxCountyLoanLimit = null;
        internal TextBox txtHardCost = null;
        internal TextBox txtContingencyReserve = null;
        internal TextBox txtArchitectEngineerFees = null;
        internal TextBox txtPlanReviewConsultantFee = null;
        internal TextBox txtInspections = null;
        internal TextBox txtTitleUpdates = null;
        internal TextBox txtPermits = null;
        internal TextBox txtPaymentReserve = null;
        internal TextBox txtDrawAdminFee = null;
        internal TextBox txtTotalRenovationBudget = null;
        internal TextBox txtRenovationFinanceFees = null;
        internal TextBox txtTotalofPurchasePriceImprovements = null;
        internal TextBox txtFinalAsCompletedValue = null;
        internal TextBox txtMaxAllowableBaseLoanAmount = null;
        internal TextBox txtTotalOfFinancedPMI = null;
        internal TextBox txtMaxAllowableTotalLoanAmount = null;
        internal TextBox txtFinalBaseLoanAmount = null;
        internal TextBox txtTotalAlterationImprovementsRepairs = null;
        internal TextBox txtBorrowersOwnFundsForContingencyReserves = null;
        internal TextBox txtRehabilitationEscrowAccountTot = null;
        internal TextBox txtTotalContingency = null;
        #endregion

        FormCalculations Fcalc = new FormCalculations();

        public InputForm()
        {
            this.Load += new EventHandler(Form_load);

        }
        private void Form_load(object sender, EventArgs e)
        {

            if (EncompassApplication.CurrentLoan.Fields["19"].FormattedValue == "Purchase")
            {
                RefinaceGroupBoxVisible(false);
                PurchaseGroupBoxVisible(true);
            }
            else
            {
                RefinaceGroupBoxVisible(true);
                PurchaseGroupBoxVisible(false);
            }
           
            if (EncompassApplication.CurrentLoan.Fields["19"].FormattedValue != "Purchase")
            {
                Fcalc.RefinanceSectionA_Field2Calculation();
                Fcalc.RefinanceSectionB_Field2Calculation();
                Fcalc.RefinanceSectionD_Field1Calculation();
                Fcalc.RefinanceSectionD_Field2Calculation();
                //Fcalc.RefinanceSectionD_Field4Calculation();
                //Fcalc.RefinanceSectionD_Field5Calculation();
                Fcalc.RefinanceSectionE_Field8Calculation();
                Fcalc.RefinanceSectionE_Field11Calculation();
                Fcalc.RefinanceSectionF_Field3Calculation();
                Fcalc.RefinanceSectionF_Field4Calculation();
            }
            else
            {
                Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
                Fcalc.PurchaseSectionA_Field2Calculation();
                Fcalc.PurchaseSectionA_TextBox1ColorChange(TextBox2);
                Fcalc.PurchaseSectionB_Field2Calculation();
                Fcalc.PurchaseSectionD_Field1Calculation();
                Fcalc.PurchaseSectionD_Field3Calculation();
                Fcalc.PurchaseSectionD_Field5Calculation();
                Fcalc.PurchaseSectionE_Field3Calculation();
                Fcalc.PurchaseSectionE_Field4Calculation();
            }


        }

        private void RefinaceGroupBoxVisible(bool visiblestatus)
        {
            GroupBox7.Visible = visiblestatus;
            GroupBox8.Visible = visiblestatus;
            GroupBox9.Visible = visiblestatus;
            GroupBox10.Visible = visiblestatus;
            GroupBox11.Visible = visiblestatus;
            GroupBox12.Visible = visiblestatus;
        }
        private void PurchaseGroupBoxVisible(bool visiblestatus)
        {
            GroupBox2.Visible = visiblestatus;
            GroupBox3.Visible = visiblestatus;
            GroupBox4.Visible = visiblestatus;
            GroupBox5.Visible = visiblestatus;
            GroupBox6.Visible = visiblestatus;
        }
        public override void CreateControls()
        {
            #region RefinanceGroupControl
            GroupBox7 = FindControl(FC.ControlIds.GroupBox7) as GroupBox;
            GroupBox8 = FindControl(FC.ControlIds.GroupBox8) as GroupBox;
            GroupBox9 = FindControl(FC.ControlIds.GroupBox9) as GroupBox;
            GroupBox10 = FindControl(FC.ControlIds.GroupBox10) as GroupBox;
            GroupBox11 = FindControl(FC.ControlIds.GroupBox11) as GroupBox;
            GroupBox12 = FindControl(FC.ControlIds.GroupBox12) as GroupBox;
            #endregion
            #region PurchaseGroupControl
            GroupBox2 = FindControl(FC.ControlIds.GroupBox2) as GroupBox;
            GroupBox3 = FindControl(FC.ControlIds.GroupBox3) as GroupBox;
            GroupBox4 = FindControl(FC.ControlIds.GroupBox4) as GroupBox;
            GroupBox5 = FindControl(FC.ControlIds.GroupBox5) as GroupBox;
            GroupBox6 = FindControl(FC.ControlIds.GroupBox6) as GroupBox;
            #endregion
            #region PurchaseTextboxControl

            TextBox2 = FindControl(FC.ControlIds.TextBox2) as TextBox;
            TextBox2.Change += new EventHandler(PurchaseSectionA_TextBox1ColorChangeEvent);

            txtPurchaseApplicableLTV = FindControl(FC.ControlIds.txtPurchasePrice) as TextBox;
            txtPurchaseApplicableLTV.Change += new EventHandler(PurchaseApplicableLTVChangeEvent);

            txtPurchasePrice = FindControl(FC.ControlIds.txtPurchasePrice) as TextBox;
            txtPurchasePrice.Change += new EventHandler(PurchasePriceChangeEvent);

            txtEstimatedValue = FindControl(FC.ControlIds.txtEstimatedValue) as TextBox;
            txtEstimatedValue.Change += new EventHandler(EstimatedValueChangeEvent);

            txtAfterImprovedValue = FindControl(FC.ControlIds.txtAfterImprovedValue) as TextBox;
            txtAfterImprovedValue.Change += new EventHandler(AfterImprovedValueChangeEvent);

            txtMaxCountyLoanLimit = FindControl(FC.ControlIds.txtMaxCountyLoanLimit) as TextBox;
            txtMaxCountyLoanLimit.Change += new EventHandler(MaxCountyLoanLimitChangeEvent);

            txtHardCost = FindControl(FC.ControlIds.txtHardCost) as TextBox;
            txtHardCost.Change += new EventHandler(HardCostChangeEvent);

            txtContingencyReserve = FindControl(FC.ControlIds.txtContingencyReserve) as TextBox;
            txtContingencyReserve.Change += new EventHandler(ContingencyReserveChangeEvent);

            txtArchitectEngineerFees = FindControl(FC.ControlIds.txtArchitectEngineerFees) as TextBox;
            txtArchitectEngineerFees.Change += new EventHandler(ArchitectEngineerFeesChangeEvent);

            txtPlanReviewConsultantFee = FindControl(FC.ControlIds.txtPlanReviewConsultantFee) as TextBox;
            txtPlanReviewConsultantFee.Change += new EventHandler(PlanReviewConsultantFeeChangeEvent);

            txtInspections = FindControl(FC.ControlIds.txtInspections) as TextBox;
            txtInspections.Change += new EventHandler(InspectionsChangeEvent);

            txtTitleUpdates = FindControl(FC.ControlIds.txtTitleUpdates) as TextBox;
            txtTitleUpdates.Change += new EventHandler(TitleUpdatesChangeEvent);

            txtPermits = FindControl(FC.ControlIds.txtPermits) as TextBox;
            txtPermits.Change += new EventHandler(PermitsChangeEvent);

            txtPaymentReserve = FindControl(FC.ControlIds.txtPaymentReserve) as TextBox;
            txtPaymentReserve.Change += new EventHandler(PaymentReserveChangeEvent);

            txtDrawAdminFee = FindControl(FC.ControlIds.txtDrawAdminFee) as TextBox;
            txtDrawAdminFee.Change += new EventHandler(DrawAdminFeeChangeEvent);
            
            txtTotalRenovationBudget = FindControl(FC.ControlIds.txtTotalRenovationBudget) as TextBox;
            txtTotalRenovationBudget.Change += new EventHandler(TotalRenovationBudgetChangeEvent);

            txtRenovationFinanceFees = FindControl(FC.ControlIds.txtRenovationFinanceFees) as TextBox;
            txtRenovationFinanceFees.Change += new EventHandler(RenovationFinanceFeesChangeEvent);

            txtTotalofPurchasePriceImprovements = FindControl(FC.ControlIds.txtTotalofPurchasePriceImprovements) as TextBox;
            txtFinalAsCompletedValue = FindControl(FC.ControlIds.txtFinalAsCompletedValue) as TextBox;
            txtFinalAsCompletedValue.Change += new EventHandler(FinalAsCompletedValueChangeEvent);

            txtMaxAllowableBaseLoanAmount = FindControl(FC.ControlIds.txtMaxAllowableBaseLoanAmount) as TextBox;
            txtMaxAllowableBaseLoanAmount.Change += new EventHandler(MaxAllowableBaseLoanAmountChangeEvent);

            txtTotalOfFinancedPMI = FindControl(FC.ControlIds.txtTotalOfFinancedPMI) as TextBox;
            txtTotalOfFinancedPMI.Change += new EventHandler(txtTotalOfFinancedPMIChangeEvent);
            //
            txtMaxAllowableTotalLoanAmount = FindControl(FC.ControlIds.txtMaxAllowableTotalLoanAmount) as TextBox;
            txtFinalBaseLoanAmount = FindControl(FC.ControlIds.txtFinalBaseLoanAmount) as TextBox;

            txtTotalAlterationImprovementsRepairs = FindControl(FC.ControlIds.txtTotalAlterationImprovementsRepairs) as TextBox;
            txtTotalAlterationImprovementsRepairs.Change += new EventHandler(TotalAlterationImprovementsRepairsChangeEvent);

            txtBorrowersOwnFundsForContingencyReserves = FindControl(FC.ControlIds.txtBorrowersOwnFundsForContingencyReserves) as TextBox;
            txtBorrowersOwnFundsForContingencyReserves.Change += new EventHandler(BorrowersOwnFundsForContingencyReservesChangeEvent);

            txtRehabilitationEscrowAccountTot = FindControl(FC.ControlIds.txtRehabilitationEscrowAccountTot) as TextBox;
            txtRehabilitationEscrowAccountTot.Change += new EventHandler(RehabilitationEscrowAccountTotChangeEvent);

            txtTotalContingency = FindControl(FC.ControlIds.txtTotalContingency) as TextBox;
            txtTotalContingency.Change += new EventHandler(TotalContingencyChangeEvent);

            #endregion

            #region RefinanceTextBox
            txtRefinanceApplicableLTV = FindControl(FC.ControlIds.txtRefinanceApplicableLTV) as TextBox;
            txtRefinanceApplicableLTV.Change += new EventHandler(RefinanceApplicableLTVChangeEvent);

            //txtRenoCostPCT = FindControl(FC.ControlIds.TextBox2) as TextBox;
            //TextBox2.Change += new EventHandler(PurchaseSectionA_TextBox1ColorChangeEvent);
            txtREFPayoffBalance = FindControl(FC.ControlIds.txtREFPayoffBalance) as TextBox;
            txtREFPayoffBalance.Change += new EventHandler(REFPayoffBalanceChangeEvent);

            txtRefEstimatedValue = FindControl(FC.ControlIds.txtRefEstimatedValue) as TextBox;
            txtRefEstimatedValue.Change += new EventHandler(RefEstimatedValueChangeEvent);

            txtRefAfterImprovedValue = FindControl(FC.ControlIds.txtRefAfterImprovedValue) as TextBox;
            txtRefAfterImprovedValue.Change += new EventHandler(RefAfterImprovedValueChangeEvent);

            txtRefMaxCountyLoanLimit = FindControl(FC.ControlIds.txtRefMaxCountyLoanLimit) as TextBox;
            txtRefMaxCountyLoanLimit.Change += new EventHandler(RefMaxCountyLoanLimitChangeEvent);

            txtRefHardCost = FindControl(FC.ControlIds.txtRefHardCost) as TextBox;
            txtRefHardCost.Change += new EventHandler(RefHardCostChangeEvent);

            txtRefContingencyReserve = FindControl(FC.ControlIds.txtRefContingencyReserve) as TextBox;
            txtRefContingencyReserve.Change += new EventHandler(RefContingencyReserveChangeEvent);

            txtRefArchitectEngineerFees = FindControl(FC.ControlIds.txtRefArchitectEngineerFees) as TextBox;
            txtRefArchitectEngineerFees.Change += new EventHandler(RefArchitectEngineerFeesChangeEvent);

            txtRefPlanReviewConsultantFee = FindControl(FC.ControlIds.txtRefPlanReviewConsultantFee) as TextBox;
            txtRefPlanReviewConsultantFee.Change += new EventHandler(RefPlanReviewConsultantFeeChangeEvent);

            txtRefInspections = FindControl(FC.ControlIds.txtRefInspections) as TextBox;
            txtRefInspections.Change += new EventHandler(RefInspectionsChangeEvent);

            txtRefTitleUpdates = FindControl(FC.ControlIds.txtRefTitleUpdates) as TextBox;
            txtRefTitleUpdates.Change += new EventHandler(RefTitleUpdatesChangeEvent);

            txtRefPermits = FindControl(FC.ControlIds.txtRefPermits) as TextBox;
            txtRefPermits.Change += new EventHandler(RefPermitsChangeEvent);

            txtRefPaymentReserve = FindControl(FC.ControlIds.txtRefPaymentReserve) as TextBox;
            txtRefPaymentReserve.Change += new EventHandler(RefPaymentReserveChangeEvent);

            txtRefDrawAdminFee = FindControl(FC.ControlIds.txtRefDrawAdminFee) as TextBox;
            txtRefDrawAdminFee.Change += new EventHandler(RefDrawAdminFeeChangeEvent);

            txtRefTotalRenovationBudget = FindControl(FC.ControlIds.txtRefTotalRenovationBudget) as TextBox;
            txtRefTotalRenovationBudget.Change += new EventHandler(RefTotalRenovationBudgetChangeEvent);

            txtRefRenovationFinanceFees = FindControl(FC.ControlIds.txtRefRenovationFinanceFees) as TextBox;
            txtRefRenovationFinanceFees.Change += new EventHandler(RefRenovationFinanceFeesChangeEvent);

            txtRefTotalFundsRequired = FindControl(FC.ControlIds.txtRefTotalFundsRequired) as TextBox;
            txtRefTotalFundsRequired.Change += new EventHandler(RefTotalFundsRequiredChangeEvent);
            //txtRefMaxAllowableBaseLoanAmount = FindControl(FC.ControlIds.TextBox2) as TextBox;
            //TextBox2.Change += new EventHandler(PurchaseSectionA_TextBox1ColorChangeEvent);
            //txtRefFinalBaseLoanAmount = FindControl(FC.ControlIds.TextBox2) as TextBox;
            //TextBox2.Change += new EventHandler(PurchaseSectionA_TextBox1ColorChangeEvent);
            
            txtRefTotalAlterationImprovementsRepairs = FindControl(FC.ControlIds.txtRefTotalAlterationImprovementsRepairs) as TextBox;
            txtRefTotalAlterationImprovementsRepairs.Change += new EventHandler(RefTotalAlterationImprovementsRepairsChangeEvent);
            txtRefLand = FindControl(FC.ControlIds.txtRefLand) as TextBox;
            txtRefLand.Change += new EventHandler(RefLandChangeEvent);
            txRefIncludesDebts = FindControl(FC.ControlIds.txRefIncludesDebts) as TextBox;
            txRefIncludesDebts.Change += new EventHandler(RefIncludesDebtsChangeEvent);
            txtRefEstimatedPrepaidItems = FindControl(FC.ControlIds.txtRefEstimatedPrepaidItems) as TextBox;
            txtRefEstimatedPrepaidItems.Change += new EventHandler(RefEstimatedPrepaidItemsChangeEvent);
            txtRefEstimatedClosingCosts = FindControl(FC.ControlIds.txtRefEstimatedClosingCosts) as TextBox;
            txtRefEstimatedClosingCosts.Change += new EventHandler(RefEstimatedClosingCostsChangeEvent);
            txtRefFinancedPMI = FindControl(FC.ControlIds.txtRefFinancedPMI) as TextBox;
            txtRefFinancedPMI.Change += new EventHandler(RefFinancedPMIChangeEvent);
            txtRefPMIPaidInCash = FindControl(FC.ControlIds.txtRefPMIPaidInCash) as TextBox;
            txtRefPMIPaidInCash.Change += new EventHandler(RefPMIPaidInCashChangeEvent);
            txtRefDiscount = FindControl(FC.ControlIds.txtRefDiscount) as TextBox;
            txtRefDiscount.Change += new EventHandler(RefDiscountChangeEvent);

            txtRefTotalCosts = FindControl(FC.ControlIds.txtRefTotalCosts) as TextBox;
            txtRefTotalCosts.Change += new EventHandler(RefTotalCostsChangeEvent);
            //txtRefSubordinateFinancing = FindControl(FC.ControlIds.TextBox2) as TextBox;
            //TextBox2.Change += new EventHandler(PurchaseSectionA_TextBox1ColorChangeEvent);
            txtRefOtherCredits = FindControl(FC.ControlIds.txtRefOtherCredits) as TextBox;
            txtRefOtherCredits.Change += new EventHandler(RefOtherCreditsChangeEvent);

            txtRefTotalFundsRequired1 = FindControl(FC.ControlIds.txtRefTotalFundsRequired1) as TextBox;
            txtRefTotalFundsRequired1.Change += new EventHandler(RefTotalFundsRequired1ChangeEvent);

            txtRefTotalAlterationImprovementsRepairs1 = FindControl(FC.ControlIds.txtRefTotalAlterationImprovementsRepairs1) as TextBox;
            txtRefTotalAlterationImprovementsRepairs1.Change += new EventHandler(RefTotalAlterationImprovementsRepairs1ChangeEvent);
            txtRefBorrowersOwnFundsForContingencyReserves = FindControl(FC.ControlIds.txtRefBorrowersOwnFundsForContingencyReserves) as TextBox;
            txtRefBorrowersOwnFundsForContingencyReserves.Change += new EventHandler(PurchaseSectionF_txtRefBorrowersOwnFundsForContingencyReserves);
            //txtRefRehabilitationEscrowAccountTot = FindControl(FC.ControlIds.TextBox2) as TextBox;
            //TextBox2.Change += new EventHandler(PurchaseSectionA_TextBox1ColorChangeEvent);
            //txtRefTotalContingency = FindControl(FC.ControlIds.TextBox2) as TextBox;
            //TextBox2.Change += new EventHandler(PurchaseSectionA_TextBox1ColorChangeEvent);
            #endregion
        }

        private void txtTotalOfFinancedPMIChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionD_Field5Calculation();
        }

        private void PurchaseSectionF_txtRefBorrowersOwnFundsForContingencyReserves(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionF_Field3Calculation();
            Fcalc.RefinanceSectionF_Field4Calculation();
        }

        private void RefTotalAlterationImprovementsRepairs1ChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field11Calculation();
            Fcalc.RefinanceSectionF_Field4Calculation();
            Fcalc.RefinanceSectionF_Field3Calculation();
        }

        private void RefOtherCreditsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field11Calculation();
        }

        private void RefTotalCostsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field11Calculation();
        }

        private void RefDiscountChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field8Calculation();
        }

        private void RefPMIPaidInCashChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field8Calculation();
        }

        private void RefFinancedPMIChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field8Calculation();
        }

        private void RefEstimatedClosingCostsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field8Calculation();
        }

        private void RefEstimatedPrepaidItemsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field8Calculation();
        }

        private void RefLandChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field8Calculation();
        }

        private void RefTotalAlterationImprovementsRepairsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionE_Field8Calculation();
        }

        private void RefIncludesDebtsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionD_Field1Calculation();
            Fcalc.RefinanceSectionE_Field8Calculation();
        }

        private void RefMaxCountyLoanLimitChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionD_Field2Calculation();
        }

        private void REFPayoffBalanceChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionD_Field1Calculation();
        }

        private void RefRenovationFinanceFeesChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefDrawAdminFeeChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefPaymentReserveChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefPermitsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefTitleUpdatesChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefInspectionsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefPlanReviewConsultantFeeChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefArchitectEngineerFeesChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefContingencyReserveChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
            Fcalc.RefinanceSectionF_Field4Calculation();
        }

        private void RefHardCostChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void RefTotalFundsRequired1ChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionB_Field2Calculation();
            
        }

        private void RefTotalFundsRequiredChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionB_Field2Calculation();
            Fcalc.RefinanceSectionD_Field2Calculation();
        }

        private void RefinanceApplicableLTVChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionB_Field2Calculation();
            Fcalc.RefinanceSectionD_Field2Calculation();
        }

        private void RefTotalRenovationBudgetChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionA_Field2Calculation();
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
            Fcalc.RefinanceSectionD_Field1Calculation();
        }

        private void RefAfterImprovedValueChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionA_Field2Calculation();
            Fcalc.RefinanceSectionD_Field2Calculation();
        }

        private void RefEstimatedValueChangeEvent(object sender, EventArgs e)
        {
            Fcalc.RefinanceSectionA_Field2Calculation();
            Fcalc.RefinanceSectionD_Field2Calculation();
        }

        private void TotalContingencyChangeEvent(object sender, EventArgs e)
        {

        }

        private void RehabilitationEscrowAccountTotChangeEvent(object sender, EventArgs e)
        {

        }

        private void BorrowersOwnFundsForContingencyReservesChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionE_Field3Calculation();
            Fcalc.PurchaseSectionE_Field4Calculation();
        }

        private void TotalAlterationImprovementsRepairsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionE_Field3Calculation();
        }

        private void MaxAllowableBaseLoanAmountChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionD_Field5Calculation();
        }

        private void FinalAsCompletedValueChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionD_Field3Calculation();
        }

        private void MaxCountyLoanLimitChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionD_Field3Calculation();
        }

        private void PurchaseApplicableLTVChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionD_Field3Calculation();
        }

        private void PurchasePriceChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionB_Field2Calculation();
            Fcalc.PurchaseSectionD_Field1Calculation();
        }

        private void RenovationFinanceFeesChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void DrawAdminFeeChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void PaymentReserveChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void PermitsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void TitleUpdatesChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void InspectionsChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void PlanReviewConsultantFeeChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void ArchitectEngineerFeesChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void ContingencyReserveChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
            Fcalc.PurchaseSectionE_Field4Calculation();
        }

        private void HardCostChangeEvent(object sender, EventArgs e)
        {
            Fcalc.SectionC_CalculationForBothPurchaseAndRefinance();
        }

        private void TotalRenovationBudgetChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionA_Field2Calculation();
            Fcalc.PurchaseSectionA_TextBox1ColorChange(TextBox2);
            Fcalc.PurchaseSectionB_Field2Calculation();
            Fcalc.PurchaseSectionD_Field1Calculation();
            Fcalc.PurchaseSectionE_Field3Calculation();
        }

        private void AfterImprovedValueChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionA_Field2Calculation();
            Fcalc.PurchaseSectionA_TextBox1ColorChange(TextBox2);
            Fcalc.PurchaseSectionD_Field3Calculation();
            
        }

        private void EstimatedValueChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionA_Field2Calculation();
            Fcalc.PurchaseSectionA_TextBox1ColorChange(TextBox2);
            Fcalc.PurchaseSectionD_Field3Calculation();
            Fcalc.PurchaseSectionD_Field1Calculation();
        }

        private void PurchaseSectionA_TextBox1ColorChangeEvent(object sender, EventArgs e)
        {
            Fcalc.PurchaseSectionA_TextBox1ColorChange(TextBox2);
        }
    }
}
